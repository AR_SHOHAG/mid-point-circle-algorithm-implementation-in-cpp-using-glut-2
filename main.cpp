#include <windows.h>
#include <iostream>
#include <math.h>
#include <time.h>
#include <GL/glut.h>

using namespace std;


void init(){
    glClearColor(0.0,0.0,0.0,0.0);
     glMatrixMode(GL_PROJECTION);

     glLoadIdentity();

     gluOrtho2D(0.0, 500.0, 0.0, 400.0);
}

void middy_circle(float r, float xc, float yc){
    float p = 1-r;
    float x = 0, y = r;
    glBegin(GL_POINTS);
        while(y>x){
            if(p<0)
            {
                p+=(2*x)+3;
                x++;
            }
            else if(p>=0)
            {
                p+=(2*x)-(2*y)+5;
                x++;
                y--;
            }
            glVertex2i((x+xc), (y+yc));
            glVertex2i((y+xc), (x+yc));
            glVertex2i((x+xc), (-y+yc));
            glVertex2i((y+xc), (-x+yc));
            glVertex2i((-x+xc), (-y+yc));
            glVertex2i((-y+xc), (-x+yc));
            glVertex2i((-x+xc), (y+yc));
            glVertex2i((-y+xc), (x+yc));
        }
        glEnd();
        glFlush();
}

void circle(){
        glClear(GL_COLOR_BUFFER_BIT);
        glColor3f(1.0f,1.0f,1.0f) ;
        glPointSize(2.0);

        middy_circle(150,300,200);
}


int main(int argc, char** argv){
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
    glutInitWindowSize(640,480);
    glutInitWindowPosition(200,200);
    glutCreateWindow("Mid Point Circle By ARIF");
    glutDisplayFunc(circle);

    init();
    glutMainLoop();
    return 0;
}
